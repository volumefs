Name: volumefs
Version: 1.0
Release: 1%{?dist}
Summary: FUSE filesystem for showing all currently mounted volumes
Group: System Environment/Base
License: GPL
URL: https://git.javispedro.com/cgit/volumefs.git/
Source0: %{name}-%{version}.tar.gz
BuildRequires: pkgconfig(fuse)
BuildRequires: pkgconfig(udisks2)
%if 0%{?suse_version} >= 1210
BuildRequires: systemd-rpm-macros
%endif

%description
volumefs is a FUSE filesystem that will be automatically populated with
symlinks to all currently mounted external filesystems, as detected by
UDisks2.

%package -n userfs
Summary: FUSE filesystem for showing home dirs of all users
Group: System Environment/Base

%description -n userfs
userfs is a FUSE filesystem that will be automatically populated with
symlinks to all the home directories of users defined in the current system.

%prep
%autosetup

%build
%configure
%make_build

%install
%make_install
install -D -m 644 scripts/volumefs.service %{buildroot}%{_unitdir}/volumefs.service
install -D -m 644 scripts/userfs.service %{buildroot}%{_unitdir}/userfs.service

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_sbindir}/volumefs
%{_unitdir}/volumefs.service

%pre
%service_add_pre volumefs.service

%post
%service_add_post volumefs.service

%preun
%service_del_preun volumefs.service

%postun
%service_del_postun volumefs.service

%files -n userfs
%defattr(-,root,root)
%{_sbindir}/userfs
%{_unitdir}/userfs.service

%pre -n userfs
%service_add_pre userfs.service

%post -n userfs
%service_add_post userfs.service

%preun -n userfs
%service_del_preun userfs.service

%postun -n userfs
%service_del_postun userfs.service


%changelog


